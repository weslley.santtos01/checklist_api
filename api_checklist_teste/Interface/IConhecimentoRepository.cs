﻿using api_checklist_teste.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_checklist_teste
{
    public interface IConhecimentoRepository
    {
        void Adicionar(Conhecimento item);
        IEnumerable<Conhecimento> Listar();
        Conhecimento Obter(int Id);
        void Atualizar(Conhecimento item);
        void Deletar(Conhecimento item);
    }
}
