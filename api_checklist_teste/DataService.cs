﻿using api_checklist_teste.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_checklist_teste
{
    public class DataService : IDataService
    {
        private readonly CheckListContext contexto;

        public DataService(CheckListContext contexto)
        {
            this.contexto = contexto;
        }

        public void InicializaDB()
        {
            contexto.Database.EnsureCreated();
        }
    }
}
