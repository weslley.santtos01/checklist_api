﻿using api_checklist_teste.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_checklist_teste.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ConhecimentoController : ControllerBase
    {
        private readonly IConhecimentoRepository _dao;

        public ConhecimentoController(IConhecimentoRepository dao)
        {
            _dao = dao;
        }

        [HttpPost]
        public ActionResult Post(Conhecimento item)
        {
            try
            {
                item.Data = DateTime.Now;
                _dao.Adicionar(item);
                return Ok("Conhecimento cadastro com sucesso!");
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpGet]
        [Route("listar")]
        public IEnumerable<Conhecimento> Listar()
        {
            try
            {
                return _dao.Listar();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpGet]
        [Route("obter")]
        public Conhecimento Obter(int id)
        {
            try
            {
                return _dao.Obter(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("atualizar")]
        public ActionResult Atualizar(Conhecimento item)
        {
            item.Data = DateTime.Now;
            _dao.Atualizar(item);
            return Ok("Conhecimento atualizado com sucesso.");
        }

        [HttpDelete]
        public ActionResult Deletar(Conhecimento item)
        {
            _dao.Deletar(item);
            return Ok("Conhecimento deletado com sucesso.");
        }


    }
}
