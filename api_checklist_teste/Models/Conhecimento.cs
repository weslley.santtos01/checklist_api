﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_checklist_teste.Models
{
    public class Conhecimento
    {
        public int ConhecimentoId { get; set; }
        public string Ambiente { get; set; }
        public string Autor { get; set; }
        public DateTime? Data { get; set; }
    }
}
