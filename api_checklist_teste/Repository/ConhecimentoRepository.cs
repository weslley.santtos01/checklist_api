﻿using api_checklist_teste.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_checklist_teste.Repository
{
    public class ConhecimentoRepository : IConhecimentoRepository
    {
        private DbContext _dbContext;

        public ConhecimentoRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Adicionar(Conhecimento item)
        {
            _dbContext.Add(item);
            _dbContext.SaveChanges();
        }

        public IEnumerable<Conhecimento> Listar()
        {
            return _dbContext.Set<Conhecimento>();
        }

        public Conhecimento Obter(int Id)
        {
            return _dbContext.Find<Conhecimento>(Id);
        }

        public void Atualizar(Conhecimento item)
        {
            _dbContext.Entry(item).State = EntityState.Modified;
            _dbContext.Update(item);
            _dbContext.SaveChanges();
        }

        public void Deletar(Conhecimento item)
        {
            _dbContext.Remove(item);
            _dbContext.SaveChanges();
        }
    }
}
