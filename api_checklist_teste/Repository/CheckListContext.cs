﻿using api_checklist_teste.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_checklist_teste.Repository
{
    public class CheckListContext : DbContext
    {
        DbSet<Conhecimento> Conhecimentos { get; set; }

        public CheckListContext(DbContextOptions options): base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Conhecimento>()
                .ToTable("Conhecimento");
        }
    }
}
